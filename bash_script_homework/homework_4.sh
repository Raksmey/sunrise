#!/bin/bash
directory="/d/sunrise_lab/sunrise/bash_script_homework"
extension=".php"
# Check if directory exists
if [ -d "$directory" ]; then
    # Filter files with the specified extension
    filtered_files=$(find . -maxdepth 1 -type f -name "*$extension")
    # Print filtered files
    if [ -n "$filtered_files" ]; then
        echo "$filtered_files"
        rm -f $filtered_files
        echo "Files wash deleted"
    else
        echo "No files with extension $extension found in $directory"
    fi
fi
