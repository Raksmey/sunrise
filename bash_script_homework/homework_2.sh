#!/bin/bash
define_zero=0
for n in {2..12}
do
    if [ -d /d/sunrise_lab/sunrise/bash_script_homework/LAB$n ]; then
        mv /d/sunrise_lab/sunrise/bash_script_homework/LAB$n /d/sunrise_lab/sunrise/bash_script_homework/LAB$define_zero$n;
    fi
done