#!/bin/bash
define_zero=0
for n in {2..12}
do
    if [ -d /d/sunrise_lab/sunrise/bash_script_homework/LAB$define_zero$n ]; then
        curr_path="/d/sunrise_lab/sunrise/bash_script_homework/LAB$define_zero$n"
        lowercase_dir_name=$(basename $curr_path | tr  'A-Z' 'a-z') 
        mv $curr_path /d/sunrise_lab/sunrise/bash_script_homework/$lowercase_dir_name
    fi
done