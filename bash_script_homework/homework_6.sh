#!/bin/bas

#A===============================
echo "Currently logged-in users:"
whoami

#B===============================
directory="/d/sunrise_lab/sunrise/bash_script_homework"
# Check if directory exists
if [ -d "$directory" ]; then
    echo "Number of files: $(find "$directory" -type f | wc -l)"
    echo "Number of directories: $(find "$directory" -mindepth 1 -type d | wc -l)"
    echo "Total size: $(du -sh "$directory" | cut -f1)"
fi

#C ===========================
 echo "Home Directory Information: $HOME"


#D ===========================
# Get the operating system name
os_name=$(uname -s)
# Get the operating system version
os_version=$(uname -r)
# Print the operating system name and version
echo "Operating System: $os_name"
echo "Version: $os_version"



#E ====================================
# Get the current working directory
current_dir=$(pwd)
# Print the current working directory
echo "Current Working Directory: $current_dir"


# F. Number of users logged in
num_users=$(whoami | wc -l)
echo "Number of users logged in: $num_users"


# G. Show all available shells in your system
# Here are script for linux only and windows I can't find, so teacher pls kinldy share this script. Thank you
echo "Available shells in the system:"
cat /etc/shells


#H ===================================
echo "Hard Disk Information:"
echo "----------------------"
# Display disk usage statistics
df -h


#I =================================
# Display CPU information using /proc/cpuinfo
if [ -f /proc/cpuinfo ]; then
    cat /proc/cpuinfo
else
    echo "Error: /proc/cpuinfo not found."
fi


#J =================================
# Display memory information using /proc/meminfo file
echo "Using /proc/meminfo file:"
cat /proc/meminfo

#K ================================
# Display detailed file system information using stat command
echo "Detailed File System Information:"
stat -f /



#L ===============================
echo "Currently Running Processes:"
echo "-------------------------------------------------------"
# Display currently running processes using ps command
ps aux



