#!/bin/bash
directory="/d/sunrise_lab/sunrise/bash_script_homework"
echo "Please enter your file name"
read -p "File name: " fname
echo $fname

# Check if directory exists
if [ -d "$directory" ]; then
   # Search for the filename in the directory and its subdirectories
    found_files=$(find "$directory" -type f -name "$fname")
    # Print the found files
    if [ -n "$found_files" ]; then
        echo "Found files with name $fname:"
        echo "$found_files"
    else
        echo "No files found with name $fname in $directory"
    fi
fi
